FROM node:16.13.1-alpine

ENV NODE_ENV=production

RUN mkdir /app
WORKDIR /app
ARG DOCKER_HOST=00.00.00.00
COPY ["package.json", "package-lock.json*", "./"]

RUN rm -Rf .nuxt

# Install app dependencies
RUN npm install
RUN npm run build

COPY . .

RUN sed -i "s/127.0.0.1/${DOCKER_HOST}/g" package.json

EXPOSE 3000

RUN npm run generate

# set the startup command to execute
CMD ["npm", "start"]