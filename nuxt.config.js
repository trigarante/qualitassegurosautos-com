module.exports = {
  target: "static",
  /*
   ** Headers of the page
   */
  head: {
    title: "qualitassegurosautos.com",
    htmlAttrs: { lang: "es-MX" },
    meta: [
      { charset: "utf-8" },
      { hid: "robots", name: "robots", content: "index, follow" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Quálitas" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    //analyze:true,
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          exclude: /(node_modules)/,
        });
      }
    },
  },

  // include css
  css: ["static/css/bootstrap.min.css", "static/css/styles.min.css"],
  plugins: [
    // '~plugins/bootstrap.js',
    { src: "~/plugins/filters.js", ssr: false },
    { src: '~/plugins/apm-rum.js', ssr: false },
  ],
  modules: ["@nuxtjs/axios",'nuxt-datadog-trace'],
  // gtm: { id: "GTM-P93GVXX" },
  env: {
    tokenData: "mHf/0x8xqWmYlrjaRWECOzmkksnuNDZv1fBvMLjpI2g=", //TOKEN DATA 
    catalogo: "https://dev.ws-qualitas.com", //CATALOGO
    sitio: "https://p.qualitassegurosautos.com", // SITIO
    coreBranding: "https://dev.core-brandingservice.com", // CORE
    motorCobro: "https://p.qualitassegurosautos.com/comprasegura/cliente/",// MOTOR DE COBRO
    urlValidaciones: "https://core-blacklist-service.com/rest", //PRODUCCIÓN
    promoCore: "https://dev.core-persistance-service.com",
    hubspot:"https://core-hubspot-dev.mark-43.net/deals/landing",
    Environment:'DEVELOP',
  },
  render: {
    http2: { push: true },
    resourceHints: false,
    compressor: { threshold: 9 },
  },
};
