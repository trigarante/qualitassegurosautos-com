// import datadogService from "~/plugins/datadog";
import Vuex from 'vuex';
import getTokenService from '../plugins/getToken';


const createStore = () => {
  const tokenData = process.env.tokenData
  return new Vuex.Store({
    state: {
      idEndPoint: '',
			status: '',
			message: '',
      cargandocotizacion: false,
      mostrarBoton: false,
      msj: false,
      msjEje: false,
      sin_token: false,
      tiempo_minimo: 1.3, //Tiempo minimo para validación de token
      config: {
        time: "",
        cotizacionAli: "",
        aseguradora: "",
        habilitarBtnEmision: false,
        habilitarBtnInteraccion: false,
        btnEmisionDisabled:false,
        cotizacion: true,
        emision: true,
        interaccion: true,
        descuento: 23,
        telefonoAS: "",
        grupoCallback: "",
        from: "",
        idPagina: 0,
        img: "logo.svg",
        alert: true,
        asgNombre: "Quálitas",
        desc: "",
        adicional: "",
        dominio: "qualitassegurosautos.com",
        domV2: "qualitassegurosautos.com",
        domV3: "qualitassegurosautos.com",
        accessToken: "",
        idCotizacionNewCore: "",
        idProductoSolicitud: "",
        textoSMS:
          "Felicidades: Ya tenemos tu cotización en breve un ejecutivo te contactará.",
        dominioCorreo: "",
        desc:'',
        msi:'',
        promoLabel:'',
        promoImg: '',
        promoSpecial:false,
        extraMsg: '',
      },
      ejecutivo: {
        nombre: "",
        correo: "",
        id: 0,
      },
      formData: {
        aseguradora: '',
        marca: '',
        modelo: '',
        descripcion: '',
        detalle: '',
        clave: '',
        cp: '',
        nombre: '',
        telefono: '',
        gclid_field:"",
        correo: '',
        edad: '',
        fechaNacimiento: '',
        genero: '',
        emailValid: '',
        telefonoValid: '',
        codigoPostalValid:'',
        dominioCorreo: "",
        urlOrigen: '',
        utmSource:'',
        utmMedium:'',
        utmCampaign:'',
        utmId:'',
        idHubspot: '',
        idLogData:'',

    },
      cotizacion: {},
    },
    actions: {
      getToken(state) {
        return new Promise((resolve, reject) => {
          getTokenService.search(tokenData).then(
            (resp) => {
              if (typeof resp.data != "undefined") {
                this.state.config.accessToken = resp.data.accessToken;
              } else {
                this.state.config.accessToken = resp.accessToken;
              }
              localStorage.setItem("authToken", this.state.config.accessToken);
              resolve(resp);
            },
            (error) => {
              reject(error);
            }
          );
        });
      },
    },
    mutations: {
      validarTokenCore: function(state) {
        try {
          if (process.browser) {
            if (
              localStorage.getItem("authToken") === null ||
              localStorage.getItem("authToken") === "undefined"
            ) {
              state.sin_token = true;
              console.log("NO HAY TOKEN...");
            } else {
              console.log("VALIDANDO TOKEN...");
              state.config.accessToken = localStorage.getItem("authToken");
              var tokenSplit = state.config.accessToken.split(".");
              var decodeBytes = atob(tokenSplit[1]);
              var parsead = JSON.parse(decodeBytes);
              var fechaUnix = parsead.exp;
              /*
               * Fecha formateada de unix a fecha normal
               * */
              var expiracion = new Date(fechaUnix * 1000);
              var hoy = new Date(); //fecha actual
              /*
               * Se obtiene el tiempo transcurrido en milisegundos
               * */
              var tiempoTranscurrido = expiracion - hoy;
              /*
               * Se obtienen las horas de diferencia a partir de la conversión de los
               * milisegundos a horas.
               * */
              var horasDiferencia =
                Math.round(
                  (tiempoTranscurrido / 3600000 + Number.EPSILON) * 100
                ) / 100;

              if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                state.sin_token = "expirado";
              }
            }
          }
        } catch (error2) {
          console.log(error2);
        }
      },
    },
  });
};
export default createStore;
