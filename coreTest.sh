#! /bin/bash
grep -rl 'https://dev.' nuxt.config.js | xargs sed -i 's/https:\/\/dev./https:\/\//g'
grep -rl 'https://p.' nuxt.config.js | xargs sed -i 's/https:\/\/p./https:\/\//g'
grep -rl '// import datadogService from "~/plugins/datadog"' store/index.js | xargs sed -i 's/\/\/ import datadogService from \"~\/plugins\/datadog\"/import datadogService from \"~\/plugins\/datadog\"/g'
grep -rl '1.0.0' plugins/datadog.js | xargs sed -i 's/1.0.0/1.0.1/g'
grep -rl 'DEVELOP' nuxt.config.js | xargs sed -i 's/DEVELOP/PRODUCTION/g'
grep -rl '_blank")' components/utilities/datosTabla.vue | xargs sed -i 's/_blank\")/_self\")/g'
#IdHubspot
grep -rl 'hubspot-dev.' nuxt.config.js | xargs sed -i 's/hubspot-dev./hubspot-prod./g'