import axios from 'axios'

let catalogo = process.env.catalogo + "/v3/qualitas-car";
class CatalogosDirectos {
  marcas() {
    return axios({
      method: "get",
      url: catalogo + '/brands'
    })
  }
  modelos(marca) {
    return axios({
      method: "get",
      url: catalogo + `/years?brand=${marca}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
  submarcas(marca, modelo) {
    return axios({
      method: "get",
      url: catalogo + `/models?brand=${marca}&year=${modelo}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    }
    );
  }
  descripciones(marca, modelo, submarca) {
    return axios({
      method: "get",
      url: catalogo + `/variants?brand=${marca}&year=${modelo}&model=${submarca}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    }
    );
  }
}

export default CatalogosDirectos;
