import axios from 'axios';

const apiURL = process.env.promoCore + '/promotions';
// Para pruebas
// const apiURL = 'https://api-promos.cf/insurers';

async function getInsurerData (accessToken, insurer='', promotype='standard') {
	try {
		const {data} = await axios({
			method: "get",
			url:`${apiURL}/${insurer}`,
			headers: { Authorization: `Bearer ${accessToken}`}
		});
				
		const promo = data.promotions.find(promo => promo.type == promotype.toUpperCase());

		return {
			name: data.name,
			promo
		}
	}
	catch(err) {
		console.log(err)
	}
}

export default getInsurerData;