import { datadogRum } from '@datadog/browser-rum';

datadogRum.init({
    applicationId: 'f3469483-0e77-4a0c-b58c-5fbce2706bd2',
    clientToken: 'pub53e90a1726d65c06520806d932415989',
    site: 'datadoghq.com',
    service:'qualitassegurosautos.com',
    env:'prod',
    // Specify a version number to identify the deployed version of your application in Datadog 
    // version: '1.0.0',
    sessionSampleRate: 20,
    sessionReplaySampleRate: 100,
    trackUserInteractions: true,
    trackResources: true,
    trackLongTasks: true,
    trackFrustrations: true,
    defaultPrivacyLevel:'allow',
       // Other configuration options...
  beforeSend: (context) => {
    // Retrieve the session ID from the cookie or generate a new one
    let sessionId = getCookie('session_id');
    if (!sessionId) {
      sessionId = generateSessionId();
      setCookie('session_id', sessionId);
    }
    // Set the session ID in the RUM data context
    context.session.id = sessionId;
  },
    
});
// Function to retrieve a cookie value by name
function getCookie(name) {
  const cookies = document.cookie.split(';');
  for (const cookie of cookies) {
    const [cookieName, cookieValue] = cookie.trim().split('=');
    if (cookieName === name) {
      return cookieValue;
    }
  }
  return null;
}
// Function to generate a new session ID
function generateSessionId() {
  // Generate a random session ID using your preferred method
  const randomId = Math.random().toString(36).substr(2, 10);
  return randomId;
}
// Function to set a cookie
function setCookie(name, value) {
  const cookie = `${name}=${value}; path=/`;
  document.cookie = cookie;
}
datadogRum.startSessionReplayRecording();

datadogRum.setGlobalContextProperty('QUALITAS', {
  typePage: 'LANDING',
  typeProject: 'BRAND',
  issueAndPay: true,
  payAndIssue: false,
});