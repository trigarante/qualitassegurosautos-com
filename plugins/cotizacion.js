// import autosService from "./ws-autos";
import axios from 'axios'

const cotizacionService = {}

cotizacionService.search = function (data, accessToken) {
  return axios({
    method: "post",
    url: process.env.promoCore + '/v2/qualitas/quotation',
    headers: { Authorization: `Bearer ${accessToken}` },
    // params: {request:data}
    data:JSON.parse(data) 
  })
}
export default cotizacionService


